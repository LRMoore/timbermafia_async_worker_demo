import logging
import timbermafia as tm

tm.basic_config(palette='synth', style='compact')

log = logging.getLogger(__name__)

log.info(f"successfully loaded {__name__}")
