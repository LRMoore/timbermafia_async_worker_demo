import asyncio
import itertools
import re
import subprocess
import uuid
import datetime
import logging

import aiofiles as aiof
import aiohttp
import timbermafia as tm


from pathlib import Path
from functools import partial


log = logging.getLogger(__name__)


async def download_worker(queue,
                          dlq,
                          session,
                          succeeded,
                          chunk_size=512_000,
                          timeout=7200,
                          on_fail_slowdown=5,
                          use_temp_filename=True):
    """ obtain a pair of url and path parameters from queue,
        download url to path and update succeeded list with local path. repeat
        until queue empty.

        in case of an issue, add url land save path to DLQ, sleep then
        go to next url in queue
    """
    worker_id = uuid.uuid4()
    log.debug(f"worker {worker_id} initialised")
    while True:
        try:
            # get a download url and desired file save path
            url, save_path = await queue.get()
            # fire off request
            path = await download_file(url,
                                       save_path,
                                       session,
                                       chunk_size,
                                       timeout,
                                       use_temp_filename)
        except (aiohttp.ClientResponseError, asyncio.TimeoutError) as e:
            log.error(f"problem with request to url {url}, moving to DLQ: {e}")
            # put args in dlq for other consumers to handle
            await dlq.put((url, save_path))
            await asyncio.sleep(on_fail_slowdown)
            log.debug(f"notifying task {len(succeeded)} done for "
                      f"{save_path.parts[-1]}")
            queue.task_done()
        except asyncio.CancelledError:
            log.debug(f"worker task {worker_id} cancelled. bye-bye!")
            raise
        else:
            # put successful path in succeeded
            succeeded.append(path)
            # notify queue
            log.debug(f"notifying task {len(succeeded)} succeeded for "
                      f"{save_path.parts[-1]}")
            queue.task_done()



async def produce(queue, items):
    "feed an iterable of items into a queue (which may have a max capacity)"
    for item in items:
        await queue.put(item)
    log.debug("all jobs in queue")


async def download_file(file_url,
                        save_path,
                        session,
                        chunk_size=512_000,
                        timeout=30,
                        use_temp_filename=True):
    """ coroutine to download a file in chunks of chunk_size at a given url,
        saving to save_path

        returns the file path upon completion for convenience
    """
    log.info(f"download file at: {file_url} to local file: {save_path}")
    async with session.get(file_url, timeout=timeout) as resp:
        total_size = int(resp.headers.get('content-length', 0))
        save_path.parent.mkdir(parents=True, exist_ok=True)
        if use_temp_filename:
            save_path_tmp = Path(str(save_path) + '.tmp')
        else:
            save_path_tmp = save_path
        async with aiof.open(save_path_tmp, "wb") as fd:
            # raise aiohttp.ClientResponseError immediately if invalid response
            resp.raise_for_status()
            log.debug(f"start writing file {save_path_tmp}")
            while True:
                chunk = await resp.content.read(chunk_size)
                if not chunk:
                    break
                await fd.write(chunk)
        if use_temp_filename:
            mv_cmd = f'mv {save_path_tmp} {save_path}'
            log.debug(f"moving temporary file...: {mv_cmd}")
            await open_subprocess(mv_cmd)
        log.info(f"download complete: {file_url} -> {save_path}")
    return save_path


async def download_files(urls,
                         save_paths,
                         session,
                         overwrite=False,
                         concurrent_download_tasks=10,
                         timeout=7200,
                         chunk_size=512_000):
    """ Download a bunch of files specified in urls to save_paths

        Use only max concurrent_download_tasks
    """
    # skip files which are already downloaded
    if not overwrite:
        not_existing_ixs = [save_paths.index(p) for p in save_paths if not p.exists()]
        save_paths = [save_paths[ix] for ix in not_existing_ixs]
        urls = [urls[ix] for ix in not_existing_ixs]
    # if nothing to download, stop here
    if not urls:
        log.info("nothing to download: download_files exiting")
        return
    # cap out max tasks at number of urls
    concurrent_download_tasks = min(concurrent_download_tasks, len(urls))
    # set up queues to hold download task args
    download_queue, download_dlq = asyncio.Queue(maxsize=2000), asyncio.Queue()
    succeeded_paths = []
    log.debug(f"create {concurrent_download_tasks} concurrent download tasks "
              f"for {len(urls)} items")
    # initialise consumers which block at queue.get() until producer fills queue
    dl_queue_consumers = [
        asyncio.create_task(
            download_worker(download_queue,
                            download_dlq,
                            session,
                            succeeded_paths,
                            timeout=timeout)
        )
        for _ in range(concurrent_download_tasks)
    ]
    log.debug(f"create {concurrent_download_tasks} dlq consumers")
    # initialise consumers for dlq
    dl_dlq_consumers = [
        asyncio.create_task(
            download_worker(download_dlq,
                            download_dlq,
                            session,
                            succeeded_paths,
                            timeout=timeout)
        )
        for _ in range(concurrent_download_tasks)
    ]
    # produce inputs for download tasks: urls and file paths
    producer = await produce(queue=download_queue, items=zip(urls, save_paths))
    # wait for all downloads in queue to get task_done
    log.debug("waiting for main download queue to complete...")
    await download_queue.join()
    # and for dlq
    log.debug("waiting for DLQ")
    await download_dlq.join()
    # cancel all coroutines once queue is empty
    log.debug("tasks done - terminating download worker coroutines")
    for consumer_future in dl_queue_consumers + dl_dlq_consumers:
        consumer_future.cancel()
    log.info(f"download_files completed successfully for "
             f"{len(succeeded_paths)}/{len(urls)} items")
    # return the paths to the downloaded files
    return succeeded_paths


async def open_subprocess(
    cmd_text,
    poll_period=1.,
    log_errors=True,
    error_log=None,#'/home/root/cmd_errs.log',
    cmd_log=None#'/home/root/cmd.log'):
):
    """ workaround async subprocess (asyncio subbprocess_shell hangs on
        communicate)
    """
    # output = await asyncio.create_subprocess_shell(
    #    cmd,
    #    stdout=asyncio.subprocess.PIPE,
    #    stderr=asyncio.subprocess.PIPE)
    log.debug("open subprocess:")
    log.debug(cmd_text)
    output = subprocess.Popen(cmd_text,
                              shell=True,
                              stdout=subprocess.PIPE,
                              stderr=subprocess.STDOUT)
    while output is not None:
        retcode = output.poll()
        if retcode is not None:
            stdout, stderr = output.communicate()
            if retcode != 0:
                if error_log:
                    with open(error_log, 'a') as fp:
                        lines = [
                            'ERROR:\n',
                            f'cmd: {cmd_text}\n',
                            f'retcode: {retcode}\n',
                            f'datetime: {str(datetime.datetime.now())}\n'
                        ]
                        if stdout:
                            lines.append(f'stdout: {stdout.decode()}\n')
                        if stderr:
                            lines.append(f'stderr: {stderr.decode()}\n')
                        fp.writelines(lines)
                err_txt = f"return code {retcode} : {stderr}"
                log.error(err_txt)
                raise OSError(err_txt)
            else:
                if cmd_log:
                    with open(cmd_log, 'a') as fp:
                        lines = [
                            'CMD:',
                            f'cmd: {cmd_text}\n',
                            f'retcode: {retcode}\n',
                            f'datetime: {str(datetime.datetime.now())}\n'
                        ]
                        if stdout:
                            lines.append(f'stdout: {stdout.decode()}\n')
                        if stderr:
                            lines.append(f'stderr: {stderr.decode()}\n')
                        fp.writelines(lines)
            if stdout:
                log.info(f'[stdout]\n{stdout.decode()}')
            if stderr:
                log.info(f'[stderr]\n{stderr.decode()}')
            # done
            return stdout.decode()
            break
        else:
            # still running? wait then check again
            await asyncio.sleep(poll_period)
