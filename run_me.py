import logging
import argparse
import asyncio
import re

import aiohttp
import timbermafia as tm

from pathlib import Path

from test_module.download import download_files

log = logging.getLogger(__name__)

# set up some download job - get Master of Puppets
DOWNLOAD_DIRECTORY = Path('.')
MASTER_OF_PUPPETS_URL = (
    'http://195.122.253.112/public/mp3/Metallica/Albums/'
    '1986%20-%20Master%20Of%20Puppets/'
)
SONGS = [
    '01%20-%20Battery.mp3',
    '02%20-%20Master%20of%20Puppets.mp3',
    '03%20-%20the%20Thing%20That%20Should%20Not%20Be.mp3',
    '04%20-%20Welcome%20Home%20(Sanitarium).mp3',
    '05%20-%20Disposable%20Heroes.mp3',
    '06%20-%20Leper%20Messiah.mp3',
    '07%20-%20Orion%20(instrumental).mp3',
    '08%20-%20Damage,%20Inc.mp3'
]


def clean_filename(f):
    return re.sub('[()]', '', f.replace('%20', '_'))


async def main_downloader(urls, paths, loop):
    async with aiohttp.ClientSession(loop=loop) as sesh:
        await download_files(urls, paths, sesh, concurrent_download_tasks=2)

URLS = [f'{MASTER_OF_PUPPETS_URL}/{s}' for s in SONGS]
SAVE_PATHS = [DOWNLOAD_DIRECTORY / Path(clean_filename(s)) for s in SONGS]


if __name__ == "__main__":

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main_downloader(URLS, SAVE_PATHS, loop))
